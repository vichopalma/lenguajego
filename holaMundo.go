package main //Definiendo el paquete main

import "fmt" // importando paquete fmt

func main() {
	/*
		Usar paquetes fmt y sus metodos
		para mostrar datos por pantalla
	*/
	fmt.Println("Hola mundo en GO!")

	fmt.Println("empezando en Go :D ")
}
