package main

import "fmt"

//CONSTANTES
const lastname = "Palma"

func main() {
	//definir variables
	var nombre string

	nombre = "Vicente"

	fmt.Println(nombre)

	nombre = "Thot"

	fmt.Println(nombre)

	//definir multiples variables
	var a, b int = 1, 2
	var c, d int

	c = 3
	d = 4

	fmt.Println(a, b, c, d)

	var (
		pi       float64
		booleano bool
		cadena   = "Texto 01"
		edad     = 29
	)

	pi = 3.141592
	booleano = true

	fmt.Println(pi, booleano, cadena, edad)

	//simplificacion definicion de variable - ":="
	v1 := 24
	v1 = 25

	v2 := "Andres"

	fmt.Println(v1, v2)

	//CONSTANTES
	const n = 33

	fmt.Println(n, lastname)

}
